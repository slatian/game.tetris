# Tetris

A simple tetris game using the cs1graphics library (cs1graphics.org) written in python

![A screenshot of the game](./screenshot.png)

## How to play

After starting the program the game immeadeatly starts.

You can control it using your usual `wasd` or the less usual `ijkl` as arrow keys.

If you hit the `space` key the falling pice will go to its final psoition.

`p` pauses the game, `R` resets the game and `Q` quits the game.

## Modding

Before talking avout the code, Apologies for the german names, this started as a school project for learning python.

### Colors

The color scheme is defined in a dictionary called `color_sceme` near the end of the file. The color names are passed to cs1graphics, so look at their documentation for a reference sheet.

### Speed

Look at the while loop at the end of the file, there is a call to the sleep function which defines the game speed.

### Shapes

To modify which shapes can fall from the sky there is an array that gets passed to the `Steingenerator` class which contains the names of the tiles in the game.

The mapping from name to shape happens in the `Stein` class in the `setType` function.

### Keybindings

The mapping from pressed keys to commands happens in the `handle` function of the `FeldZeichner` class.

The commands are interpreted in the `cmdCallback` function of the `Feld` class.
