import cs1graphics as gfx
import time
import math
import random

spalten = 10
zeilen = 15

GfxSquare_seitenlänge = 50


#GfxSquare (Tetris)
#spalte
#zeile
#darstellung
#farbe

def min(x,y):
	if x>=y:
		return y
	else:
		return x
		
def max(x,y):
	if x<=y:
		return y
	else:
		return x

class vec2:
	def __init__(self,x,y):
		self.x = x
		self.y = y
# ########################################
# ### GRAPHIK #################
# ###################

class GfxSquare:
	def __init__(self,spalte,zeile,farbe,GfxSquare_seitenlänge,offsetx=0,offsety=0,foreground=False):
		self.farbe = farbe
		self.GfxSquare_seitenlänge = GfxSquare_seitenlänge
		self.darstellung = gfx.Square(size=GfxSquare_seitenlänge)
		self.offsetx = offsetx
		self.offsety = offsety		
		self.bewegenZu(spalte,zeile)
		self.setColor(farbe)
		if(foreground):
			self.darstellung.setDepth(0)
		else:
			self.darstellung.setDepth(10)
		
	def bewegenZu(self,spalte,zeile):
		self.spalte = spalte
		self.zeile = zeile 
		#print("BZ",self.offsetx,self,offsety)
		self.darstellung.moveTo(math.floor((spalte*self.GfxSquare_seitenlänge+self.GfxSquare_seitenlänge/2))+self.offsetx,math.floor((zeile*self.GfxSquare_seitenlänge+self.GfxSquare_seitenlänge/2))+self.offsety)
		
		
	def setColor(self,color):
		self.darstellung.setFillColor(color)
		
	def bewegenUm(self,nachRechts,nachLinks):
		self.bewegenZu(self.spalte+nachRechts,self.zeile+nachLinks)	
		
	def anzeigenIn(self,canvas):
		canvas.add(self.darstellung)
		
	def removeFrom(self,canvas):
		canvas.remove(self.darstellung)

class FeldZeichner(gfx.EventHandler):
	def __init__(self,GfxSquare_seitenlänge,color_sceme,offsetx=0,offsety=0,canvas=None):
		self.initalized = False
		self.GfxSquare_seitenlänge = GfxSquare_seitenlänge
		self.colorsceme = color_sceme
		self.cmdCallback = None
		self.offsetx = offsetx
		self.offsety = offsety
		self.canvas = canvas
		
	def setCmdCallbackHandler(self,handler):
		self.cmdHandler = handler
	
	def handle(self,event):
		desc = event.getDescription()
		key	= event.getKey()
		keysym = event.getKeysym()
		
		print("event",desc,key,keysym)
		if desc == "keyboard" : #Test if cmd handler is set
			if self.cmdHandler is None:
				return
			if key=="i" or key=="w" or keysym=="Up": #up
				self.cmdHandler.cmdCallback("up")
			if key=="k" or key=="s" or keysym=="Down": #down
				self.cmdHandler.cmdCallback("down")
			if key=="j" or key=="a" or keysym=="Left": #left
				self.cmdHandler.cmdCallback("left")
			if key=="l" or key=="d" or keysym=="Right": #right
				self.cmdHandler.cmdCallback("right")
			if key==" ":
				self.cmdHandler.cmdCallback("drop")
			if key=="p":
				self.cmdHandler.cmdCallback("pause")
			if key=="R":
				self.cmdHandler.cmdCallback("reset")
			if key=="Q":
				self.cmdHandler.cmdCallback("quit")
	
	def initalize(self,spalten,zeilen,canvas=None):
		if self.initalized:
			print("already initalized")
			return #TODO: so machen, dass man reinitialisieren kann
		if canvas is None:
			self.canvas=gfx.Canvas(spalten*self.GfxSquare_seitenlänge+1,zeilen*self.GfxSquare_seitenlänge+1)
			self.canvas.setTitle("Tetris by baschdel")
		else:
			self.canvas=canvas
		self.w = spalten
		self.h = zeilen
		self.feld=[None]*zeilen
		for y in range(0,zeilen):
			self.feld[y] = [None]*spalten
			for x in range(0,spalten):
				q = GfxSquare(x,y,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety)
				self.feld[y][x] = q
				q.anzeigenIn(self.canvas)
		self.canvas.addHandler(self)
		self.steinzeichner = Steinzeichner(self.GfxSquare_seitenlänge,self.colorsceme,self.canvas,self.offsetx,self.offsety)
#		self.dtiles = [
#			GfxSquare(1,0,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety,foreground=True),
#			GfxSquare(0,1,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety,foreground=True),
#			GfxSquare(0,0,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety,foreground=True),
#			GfxSquare(1,1,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety,foreground=True),
#		]
#		for dt in self.dtiles:
#			dt.anzeigenIn(self.canvas)
	#TODO add 4 dynamic tiles
	
	def setTitle(self,title):
		self.canvas.setTitle(title)
	
	def setWorldContent(self,world):
		self.world = world
		for y in range(0,min(len(world),len(self.feld))):
			for x in range(0,min(len(world[y]),len(self.feld[y]))):
				self.feld[y][x].setColor(self.colorsceme[world[y][x]])

	def setEntitys(self,entitys):
		self.steinzeichner.drawEntitys(entitys)
#		#todo dynamic number of entitys
#		#self.setWorldContent(self.world)
#		if len(entitys) == 0:
#			for dt in dtiles:
#				dt.bewegenZu(-1,-1)
#		counter = 0
#		for e in entitys:
#			#print("ENT",e.x+e.ox,e.y+e.oy,counter)
#			self.dtiles[counter].bewegenZu(e.x+e.ox,e.y+e.oy)
#			self.dtiles[counter].setColor(self.colorsceme[e.etype])
#			#self.dtiles[counter].setColor("pink")
#			counter = counter+1
#			if counter > 4: #to prevent crashes while entity count is static
#				break
#			#WARNING: this kind of loop breaks if there are not 4 entitys
#			#self.feld[e.y+e.oy][e.x+e.ox].setColor("black")

class Steinzeichner:
	def __init__(self,GfxSquare_seitenlänge,color_sceme,canvas,offsetx=0,offsety=0):
		self.colorsceme = color_sceme
		self.GfxSquare_seitenlänge = GfxSquare_seitenlänge
		self.offsetx = offsetx
		self.offsety = offsety
		self.canvas = canvas
		self.GfxSquaree = []
		
	def drawEntitys(self,entitys):
		while len(entitys) != len(self.GfxSquaree):
			if len(entitys) > len(self.GfxSquaree):
				#GfxSquaree adden
				q = GfxSquare(1,0,self.colorsceme["init"],self.GfxSquare_seitenlänge,self.offsetx,self.offsety,foreground=True)
				q.anzeigenIn(self.canvas)
				self.GfxSquaree.append(q)
			elif len(entitys) < len(self.GfxSquaree):
				#GfxSquaree entfernen
				q = self.GfxSquaree.pop()
				q.removeFrom(self.canvas)
				del q
		counter = 0
		for e in entitys:
			#print("ENT",e.x+e.ox,e.y+e.oy,counter)
			self.GfxSquaree[counter].bewegenZu(e.x+e.ox,e.y+e.oy)
			self.GfxSquaree[counter].setColor(self.colorsceme[e.etype])
			#self.dtiles[counter].setColor("pink")
			counter = counter+1

class Scorecounter:
	def __init__(self,color_sceme,canvas,offx=3,offy=3,fontsize=40):
		self.text = gfx.Text(message="0",fontsize = fontsize)
		self.text.setFontColor(color_sceme["scorecounter"])
		self.text.moveTo(offx+fontsize*2,offy+fontsize/1.5)
		self.text.setDepth(0)
		canvas.add(self.text)
		
	def setScore(self,score):
		self.text.setMessage(str(score))
		
class Banner:
	def __init__(self,color_sceme,canvas,offx=0,offy=-1,width=-1,height=-1,fontsize=40,message=""):
		if width < 0:
			width = canvas.getWidth()-offx
		if height < 0:
			height = (canvas.getHeight())/4
		if offy == -1 :
			offy = (canvas.getHeight())/4
		self.w = width
		self.h = height
		self.text = gfx.Text(message="PAUSED",fontsize = fontsize)
		self.text.setFontColor(color_sceme["banner_message"])
		self.text.moveTo(offx+width/2,offy+height/2+fontsize/5)
		self.text.setDepth(0)
		self.bg = gfx.Rectangle(width,height,gfx.Point(offx+width/2,offy+height/2))
		self.bg.setDepth(1)
		self.bg.setFillColor(color_sceme["banner_background"])
		self.bg.setBorderColor(color_sceme["banner_background"])
		self.hidden = True
		self.canvas = canvas

	def show(self,message=None):
		if not message is None:
			self.text.setMessage(message)
		if self.hidden:
			self.canvas.add(self.text)
			self.canvas.add(self.bg)
		self.hidden = False
		
	def hide(self):
		if self.hidden :
			return
		self.canvas.remove(self.text)
		self.canvas.remove(self.bg)
		self.hidden = True
		
	def setScore(self,score):
		self.text.setMessage(str(score))
		
# #########################################
# ### MECHANIK ################
# #####################

class Entity:
	def __init__(self,x,y,etype,ox,oy):
		self.x = x	 #X position innerhalb eines steins
		self.y = y	 #Y position innerhalb eines steins
		self.ox = ox #X position des Steins
		self.oy = oy #Y position des Steins
		self.etype = etype

class Stein:
	def __init__(self,x,y,feld = None):
		self.entitys = []
		self.x = x
		self.y = y
		self.feld=feld

	def setType(self,t):
		del self.entitys
		ox = self.x
		oy = self.y
		if t == "O":
			self.entitys = [Entity(0,0,1,ox,oy),Entity(1,0,1,ox,oy),Entity(0,1,1,ox,oy),Entity(1,1,1,ox,oy)]
		elif t == "T":
			self.entitys = [Entity(1,0,2,ox,oy),Entity(1,1,2,ox,oy),Entity(2,1,2,ox,oy),Entity(0,1,2,ox,oy)]
		elif t == "I":
			self.entitys = [Entity(0,0,3,ox,oy),Entity(1,0,3,ox,oy),Entity(2,0,3,ox,oy),Entity(3,0,3,ox,oy)]
		elif t == "L":
			self.entitys = [Entity(0,0,4,ox,oy),Entity(0,1,4,ox,oy),Entity(0,2,4,ox,oy),Entity(1,2,4,ox,oy)]
		elif t == "J":
			self.entitys = [Entity(1,0,5,ox,oy),Entity(1,1,5,ox,oy),Entity(1,2,5,ox,oy),Entity(0,2,5,ox,oy)]
		elif t == "S":
			self.entitys = [Entity(0,0,6,ox,oy),Entity(1,0,6,ox,oy),Entity(1,1,6,ox,oy),Entity(2,1,6,ox,oy)]
		elif t == "Z":
			self.entitys = [Entity(0,1,7,ox,oy),Entity(1,1,7,ox,oy),Entity(1,0,7,ox,oy),Entity(2,0,7,ox,oy)]
		else:
			self.entitys = []
	
	def setPosition(self,x,y):
		self.x = x
		self.y = y
		for e in self.entitys:
			e.ox = x
			e.oy = y
	
	def bewegenUm(self,x,y):
		self.setPosition(self.x+x,self.y+y)
		
	def rotate(self):
		
		newPositions = []
		
		for e in self.entitys:
			ny = e.x
			nx = -e.y
			newPositions.append(vec2(nx,ny))
			
		offx = 0
		offy = 0
		
		for p in newPositions:
			if p.x < offx:
				offx = p.x
			if p.y < offy:
				offy = p.y
			
		avgx = 0
		avgy = 0
			
		for p in newPositions:
			p.x = p.x-offx
			p.y = p.y-offy
			avgx = avgx + p.x
			avgy = avgy + p.y
			
		avgx = avgx//len(newPositions)
		avgy = avgy//len(newPositions)
			
		for p in newPositions:
			p.x = p.x-avgx
			p.y = p.y-avgy

		if not (self.feld is None):
			for p in newPositions:
				if not (self.feld == None or self.feld.isFree(p.x+self.x,p.y+self.y)):
					return False
			
		for e in self.entitys:
			p = newPositions.pop()
			e.x = p.x
			e.y = p.y	
		return True
		
class Steingenerator:
	def __init__(self,types):
		self.next = None
		self.types = types
		self.pop()	
	
	def peek(self):
		return self.next
		
	def pop(self):
		i = random.randrange(0,len(self.types))
		n = self.next
		self.next = self.types[i]
		return n
	
class Feld:
	def __init__(self,breite,höhe,zeichner,steingenerator=None):
		self.zeichner = zeichner
		self.w = breite
		self.h = höhe
		zeichner.initalize(breite,höhe)
		self.feld=[None]*höhe
		for y in range(0,höhe):
			self.feld[y] = [None]*breite
			for x in range(0,breite):
				self.feld[y][x] = 0 #set field to be empty
		self.zeichner.setWorldContent(self.feld)
		self.stein = Stein(0,0,feld=self)
		self.zeichner.setCmdCallbackHandler(self)
		self.gameover = False
		self.paused = False
		self.quitrequested = False
		self.steingenerator = steingenerator
		self.resetStein()
		self.zeichner.setEntitys(self.stein.entitys)
		self.score = 0
	
	def isFree(self,x,y):
		if x<0 or y<0 :
			return False
		if x>=self.w or y>=self.h:
			return False
		return self.feld[y][x] == 0
	
	def canMoveSteinTo(self,stein,x,y):
		for e in stein.entitys:
			if not self.isFree(x+e.x,y+e.y):
				return False
		return True
	
	def place(self):
		for e in self.stein.entitys:
			self.feld[e.y+e.oy][e.x+e.ox] = e.etype
	
	def resetStein(self):
		t = None
		if self.steingenerator == None:
			stones = ["T","O","I","L","J","S","Z"]
			i = random.randrange(0,len(stones))
			t = stones[i]
		else:
			t = self.steingenerator.pop()
		self.stein.setType(t)
		self.stein.setPosition((math.floor(self.w/2)-1),0)
		if not self.canMoveSteinTo(self.stein,self.stein.x,self.stein.y+1):
			print("Gameover!")
			self.gameover = True
	
	def cmdCallback(self,cmd):
		print("Got a command:",cmd)
		if not (self.gameover or self.paused):
			if cmd == "up":
				self.stein.rotate()
			elif cmd == "down":
				if self.canMoveSteinTo(self.stein,self.stein.x,self.stein.y+1):
					self.stein.bewegenUm(0,1)
			elif cmd == "left":
				if self.canMoveSteinTo(self.stein,self.stein.x-1,self.stein.y):
					self.stein.bewegenUm(-1,0)
			elif cmd == "right":
				if self.canMoveSteinTo(self.stein,self.stein.x+1,self.stein.y):
					self.stein.bewegenUm(1,0)
			elif cmd == "drop":
				while self.canMoveSteinTo(self.stein,self.stein.x,self.stein.y+1):
					self.stein.bewegenUm(0,1)
			self.zeichner.setEntitys(self.stein.entitys)
		if cmd == "pause":
			self.paused = not self.paused
		if cmd == "reset":
			self.resetFeld()
		if cmd == "quit":
			self.quitrequested = True
		
	def copyRow(self,fromRow,toRow):
		x = 0 
		while x<self.w:
			self.feld[toRow][x] = self.feld[fromRow][x]
			x=x+1
		
	def clearRowAndShift(self,y):
		while y>0:
			self.copyRow(y-1,y)
			y=y-1 
		x=0
		while x<self.w:
			self.feld[0][x] = 0
			x=x+1
		
	def clearFullRows(self):
		y = 0
		bonus = 0
		while y<self.h:
			hasEmpty = False
			for q in self.feld[y]:
				if q == 0:
					hasEmpty = True
					break
			if not hasEmpty:
				self.clearRowAndShift(y)
				self.score = self.score+10+bonus
				bonus = bonus+5
			y=y+1
	
	def resetFeld(self):
		for y in range(0,len(self.feld)):
			for x in range(0,len(self.feld[y])):
				self.feld[y][x] = 0 #set field to be empty
		self.resetStein()
		self.paused = False
		self.gameover = False
		self.score = 0
		
	def tick(self):
		if self.paused:
			return
		self.clearFullRows()
		if self.canMoveSteinTo(self.stein,self.stein.x,self.stein.y+1):
			self.stein.bewegenUm(0,1)
		else:
			self.place()
			self.resetStein()
			self.score = self.score+1
		self.zeichner.setWorldContent(self.feld)
		self.zeichner.setEntitys(self.stein.entitys) 


#color_sceme = {'init':"black",'falling':"black",0:"black",1:"white",2:"white",3:"white",4:"white",5:"white",6:"white",7:"white",'scorecounter':"white",'banner_message':"white",'banner_background':"black"}
color_sceme = {'init':"pink",'falling':"pink",0:"grey10",1:"red",2:"green",3:"blue",4:"gold",5:"cyan",6:"purple",7:"hotpink2",'scorecounter':"grey100",'banner_message':"grey100",'banner_background':"black"}
g = Steingenerator(["T","O","I","L","J","S","Z"])
z = FeldZeichner(GfxSquare_seitenlänge,color_sceme)
f = Feld(spalten,zeilen,z,g)
s = Scorecounter(color_sceme,z.canvas,offx=3,offy=(GfxSquare_seitenlänge/4)-3)
b = Banner(color_sceme,z.canvas,fontsize=90,height=240)
p = Steinzeichner(GfxSquare_seitenlänge/2,color_sceme,z.canvas,offsetx=GfxSquare_seitenlänge*(spalten-2.25),offsety=GfxSquare_seitenlänge/4)

ps = Stein(0,0)

running = True
wasPaused = False


while running:
	b.show("3")
	time.sleep(1)
	b.show("2")
	time.sleep(1)
	b.show("1")
	time.sleep(1)
	b.show("GO!")
	time.sleep(0.5)
	b.hide()
	while not f.gameover and (not f.quitrequested):
		if len(gfx._graphicsManager._openCanvases) == 0:
			break
		time.sleep(0.60)
		f.tick()
		ps.setType(g.peek())
		p.drawEntitys(ps.entitys)
		s.setScore(f.score)#
		if f.paused:
			z.setTitle("Tetris ["+str(f.score)+"] [paused] press p to unpause")
		else:
			z.setTitle("Tetris ["+str(f.score)+"] use wasd or ijkl")
		if not wasPaused == f.paused:
			if f.paused:
				b.show("PAUSED")
			else:
				b.hide()
			wasPaused = f.paused
	b.show("GAME\nOVER")
	z.setTitle("Tetris ["+str(f.score)+"] Game over, press R to restart")
	while f.gameover and (not f.quitrequested):
		time.sleep(1)
		if len(gfx._graphicsManager._openCanvases) == 0:
			break
	b.hide()
	if (f.quitrequested):
		running = False
	if len(gfx._graphicsManager._openCanvases) == 0:
		break
		
print("GOODBYE")
z.canvas.close()
